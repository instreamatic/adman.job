from typing import AsyncIterable, List

from core import Queue, T, R


class SingleQueue(Queue):

    async def execute(self, source: AsyncIterable[T]) -> List[R]:
        executor = self._build_executor()
        results = []
        async for task in source:
            results.append(await executor.execute(task))
        return results


class ParallelQueue(Queue):

    async def execute(self, source: AsyncIterable[T]) -> List[R]:
        raise NotImplementedError
